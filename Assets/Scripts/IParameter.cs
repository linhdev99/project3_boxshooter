﻿interface IParameter
{
    float healthBase { get; set; }
    float healthCur { get; set; }
    int coin { get; set; }
    float speed { get; set; }
    float speedRotate { get; set; }
    float damage { get; set; }
    float point { get; set; }
    float jumpForce { get; set; }
}
