﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{
    [SerializeField]
    private LayerMask layerGround;
    [SerializeField]
    private float distanceGround = 1f;

    [SerializeField]
    private float playerSpeed = 10f;
    [SerializeField]
    private float playerSpeedRoate = 3f;
    [SerializeField]
    private float playerJumpForce = 5f;
    [SerializeField]
    private GameObject GunObj;
    private Quaternion GunRotation;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private Transform cam;
    protected override void Start()
    {
        speed = playerSpeed;
        speedRotate = playerSpeedRoate;
        GunRotation = GunObj.transform.localRotation;
        jumpForce = playerJumpForce;
        base.Start();
    }
    protected override void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        Vector3 direction = new Vector3(horizontal, 0f, vertical);
        MoveFoward(direction);
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            JumpUp();
            Debug.Log("jump");
        }
        if (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.R))
        {
            Shoot();
        }
        if (mouseX != 0)
        {
            PlayerRotate(mouseX);
        }
        if (mouseY != 0)
        {
            CameraRotate(mouseY);
        }
        base.Update();
    }
    private bool IsGrounded()
    {
        bool result = false;
        RaycastHit hit;
        if (Physics.Raycast(transform.position * 1.01f, -transform.up, out hit, distanceGround, layerGround))
        {
            if (hit.transform.CompareTag("Ground"))
            {
                result = true;
            }
        }
        Debug.DrawRay(transform.position, -transform.up * distanceGround, Color.yellow, 5f);
        return result;
    }
    private void CameraRotate(float mouseY)
    {
        float value = -mouseY * speedRotate;
        GunRotation.x += value;
        GunRotation.x = Mathf.Clamp(GunRotation.x, -25f, 20f);
        GunObj.transform.localRotation = Quaternion.Euler(GunRotation.x, GunRotation.y, GunRotation.z);
    }
    private void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 100f))
        {
            GameObject bulletGO = Instantiate(bullet, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(bulletGO, 1.5f);
        }
    }
}
