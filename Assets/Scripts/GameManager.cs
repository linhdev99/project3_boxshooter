﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public Player player;
    public GameObject creepPrefab;
    public GameObject coinPrefab;
    public List<Creep> listCreep;
    public UIManager uIManager;
    void Awake()
    {
        player.impactCreep = impactCreep;
        player.impactCoin = impactCoin;
        player.updateHealthUI = updateHealthUI;
        player.updateCoinUI = updateCoinUI;
        player.updatePoint = updatePointUI;
        player.gameover = gameover;
        spawnEnemy(10);
        spawnCoin(30);
    }
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {

    }
    void impactCreep(Creep creep)
    {
        player.takeDamage(creep.giveDamage());
    }
    void impactCoin(Coin coin)
    {
        player.collectCoin(coin.value);
        coin.PlaySound();
    }
    void updateHealthUI(float healthCur, float healthBase)
    {
        string txt = "Health: " + healthCur.ToString() + "/" + healthBase.ToString();
        uIManager.updateHealth(txt);
    }
    void updateCoinUI(int coin)
    {
        string txt = "Coin: " + coin.ToString();
        uIManager.updateCoin(txt);
    }
    void updatePointUI(float point)
    {
        string txt = "Point: " + point.ToString();
        uIManager.updatePoint(txt);
    }
    void gameover()
    {
        uIManager.GameOver();
    }
    void enemyDie(GameObject creep)
    {
        player.increasePoint(1f);
        listCreep.Remove(creep.GetComponent<Creep>());
        if (listCreep.Count < 2)
        {
            spawnEnemy(2);
        }
    }
    void spawnEnemy(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject creepGO = Instantiate(creepPrefab, new Vector3(Random.Range(-70f, 70f), 1f, Random.Range(-70f, 70f)), creepPrefab.transform.rotation);
            creepGO.GetComponent<Creep>().enemyDie = enemyDie;
            listCreep.Add(creepGO.GetComponent<Creep>());
        }
    }
    void spawnCoin(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject coinGO = Instantiate(coinPrefab, new Vector3(Random.Range(-70f, 70f), 1f, Random.Range(-70f, 70f)), coinPrefab.transform.rotation);
        }
    }
}
