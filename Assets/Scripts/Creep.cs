﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Creep : Character
{
    [SerializeField]
    private float creepHealthBase = 10f;
    [SerializeField]
    private float creepDamage = 1f;
    public AudioSource sfx;
    NavMeshAgent agent;
    GameObject player;
    protected override void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        healthBase = creepHealthBase;
        healthCur = creepHealthBase;
        damage = creepDamage;
        base.Start();
    }
    protected override void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            agent.SetDestination(player.transform.position);
        }
        base.Update();   
    }
    public void PlaySound()
    {
        sfx.Play();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            PlaySound();
            takeDamage(1f);
        }
    }
}
