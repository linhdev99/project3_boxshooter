﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Character : MonoBehaviour, IParameter
{
    public float healthBase { get; set; }
    public int coin { get; set; }
    public float speed { get; set; }
    public float healthCur { get; set; }
    public float damage { get; set; }
    public float speedRotate { get; set; }
    public float point { get; set; }
    public float jumpForce { get; set; }
    private Rigidbody rb;
    private Vector3 curDir = Vector3.zero;
    public Action<float, float> updateHealthUI;
    public Action<int> updateCoinUI;
    public Action<float> updatePoint;
    public Action<GameObject> enemyDie;
    public Action gameover;
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    protected virtual void Update()
    {

    }
    protected void MoveFoward(Vector3 dir)
    {
        Vector3 forwardMovement = transform.forward * dir.z;
        Vector3 rightMovement = transform.right * dir.x;
        Vector3 velocity = (forwardMovement + rightMovement).normalized;
        if (rb.velocity.magnitude >= speed)
        {
            rb.velocity = new Vector3(velocity.x * speed, rb.velocity.y, velocity.z * speed);
        }
        else
        {
            rb.velocity += new Vector3(velocity.x * speed / 3, 0f, velocity.z * speed / 3);
        }
    }
    protected void JumpUp()
    {
        rb.velocity += transform.up * jumpForce;
    }
    protected void PlayerRotate(float mouseX)
    {
        Vector3 eulerAngleVelocity = new Vector3(0, mouseX * speedRotate, 0);
        Quaternion deltaRotate = Quaternion.Euler(eulerAngleVelocity);
        rb.MoveRotation(rb.rotation * deltaRotate);
    }
    public void takeDamage(float val)
    {
        healthCur -= val;
        checkHealth();
    }
    public float giveDamage()
    {
        return damage;
    }
    public void collectCoin(int value)
    {
        coin = coin + value;
        if (this.gameObject.CompareTag("Player"))
        {
            updateCoinUI(coin);
        }
    }
    public void increasePoint(float value)
    {
        point = point + value;
        updatePoint(point);
    }
    protected void checkHealth()
    {
        if (healthCur >= healthBase)
        {
            healthCur = healthBase;
            return;
        }
        if (healthCur <= 0f)
        {
            healthCur = 0f;
            Die();
        }
        if (this.gameObject.CompareTag("Player"))
        {
            updateHealthUI(healthCur, healthBase);
        }
    }
    protected void Die()
    {
        if (!this.gameObject.tag.Equals("Player"))
        {
            enemyDie(this.gameObject);
            GetComponent<Collider>().enabled = false;
            StartCoroutine(waitSoundStop());
        }
        else
        {
            GetComponent<Collider>().enabled = false;
            rb.useGravity = false;
            gameover();
        }
    }

    IEnumerator waitSoundStop()
    {
        yield return new WaitForSeconds(0.35f);
        Destroy(this.gameObject);
    }
    protected void setDefaultValue()
    {
        if (this.gameObject.CompareTag("Player"))
        {
            updateHealthUI(healthCur, healthBase);
            updateCoinUI(coin);
            updatePoint(point);
        }
    }
}